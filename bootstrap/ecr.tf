# add an ECR for each application component / docker image

resource "aws_ecr_repository" "ecr_oct" {
  name = "${var.prefix}/oct"
}