image: 
  name: hashicorp/terraform:1.0.0
  entrypoint: [""]
cache:
  key: $CI_COMMIT_REF_SLUG
  
variables: &global
  TF_VAR_region: "us-east-1"
  TF_VAR_prefix: $CI_COMMIT_REF_SLUG
  TF_VAR_vpc_id: "vpc-027122e0d450bb020"
  TF_VAR_dns: "true"
  TF_VAR_domain: "bsh69.com"
  TF_VAR_user: $GITLAB_USER_NAME
  TF_VAR_orchard_app_data: "/efs/oct-dev"
  TF_VAR_efs_id: "fs-4a2784bf"
  TF_VAR_pipeline_id: "latest" # $CI_PIPELINE_IID

stages:
  - apply-state
  - apply-bootstrap
  - build
  - apply-site
  - destroy-site
  - destroy-bootstrap
  - destroy-state

Apply State:
  stage: apply-state
  image: 
    name: amazon/aws-cli:2.2.44
    entrypoint: [""]
  variables:
    <<: *global
  script:
    - aws --region us-east-2 cloudformation deploy --template-file bootstrap/state.yml --stack-name oct-terraform-state --no-fail-on-empty-changeset
  only:
    - main

.terraform_apply:
  artifacts:
    paths:
      - $folder/tf.plan
  resource_group: $CI_COMMIT_REF_SLUG-$folder
  script:
    - export DATE=`date '+%Y-%m-%d %H:%M:%S'`
    - echo "deploying $CI_COMMIT_REF_SLUG-$folder at $DATE"
    - cd $folder
    - terraform init -input=false -backend-config="key=$key"
    - terraform validate
    - terraform plan -input=false -out=tf.plan
    - terraform apply -input=false -auto-approve tf.plan

.terraform_destroy:
  resource_group: $CI_COMMIT_REF_SLUG-$folder
  script:
    - cd $folder
    - terraform init -input=false -backend-config="key=$key"
    - terraform destroy -input=false -auto-approve -refresh=false
  when: manual

Apply Bootstrap:
  extends:
    - .terraform_apply
  stage: apply-bootstrap
  variables:
    <<: *global
    key: $CI_COMMIT_REF_SLUG-bootstrap.json
    folder: bootstrap
  artifacts:
    expire_in: never
    paths:
      - bootstrap/
  dependencies:
    - Apply State

Build:
  stage: build
  variables:
    DOCKER_HOST: tcp://docker:2375
    IMAGE_NAME: $CI_COMMIT_REF_SLUG/oct
    IMAGE_TAG: "latest"
    IMAGE_FOLDER: images/oct
  image: ${CI_REGISTRY}/dev-ops-img/aws-cli-docker/aws-cli-docker:4
  services:
    - docker:20.10.9-dind-alpine3.14
  script:
    - echo "Building and Pushing ${IMAGE_NAME}:${IMAGE_TAG}"
    - cd $IMAGE_FOLDER
    - export REPO=$(aws ecr describe-repositories --repository-names "$IMAGE_NAME" --query "repositories[0].repositoryUri"  --output text)
    - docker build -t $REPO:$IMAGE_TAG .
    - aws ecr get-login-password | docker login --username AWS --password-stdin $REPO
    - docker push $REPO:$IMAGE_TAG
  rules:
    - changes:
      - $IMAGE_FOLDER/*

Apply Site:
  extends:
    - .terraform_apply
  stage: apply-site
  variables:
    <<: *global
    key: $CI_COMMIT_REF_SLUG.json
    folder: site
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.$TF_VAR_domain
    on_stop: Destroy Site
  artifacts:
    expire_in: never
    paths:
      - site/
  dependencies:
    - Apply Bootstrap

Destroy Site:
  extends:
    - .terraform_destroy
  stage: destroy-site
  variables:
    <<: *global
    key: $CI_COMMIT_REF_SLUG.json
    GIT_STRATEGY: none
    folder: site
  when: manual
  environment:
    name: $CI_COMMIT_REF_NAME
    action: stop
  dependencies:
    - Apply Site

Destroy Bootstrap:
  extends:
    - .terraform_destroy
  stage: destroy-bootstrap
  variables:
    <<: *global
    key: $CI_COMMIT_REF_SLUG-bootstrap.json
    GIT_STRATEGY: none
    folder: bootstrap
  when: manual
  dependencies:
    - Apply Bootstrap
    - Destroy Site

Destroy State:
  stage: destroy-state
  image: 
    name: amazon/aws-cli
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
  script:
    - aws s3 rm s3://oct-terraform-state --recursive
    - aws --region us-east-2 cloudformation delete-stack --stack-name oct-terraform-state
    - aws --region us-east-2 cloudformation wait stack-delete-complete --stack-name oct-terraform-state
  when: manual
  only:
    - main
  dependencies:
    - Apply State
    - Destroy Site
    - Destroy Bootstrap