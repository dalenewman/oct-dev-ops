resource "aws_lb" "alb" {
  name               = "${substr(var.prefix, 0, 28)}-alb"
  subnets            = data.aws_subnet_ids.subnets.ids
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
}

resource "aws_lb_target_group" "site_target_group" {

  name        = "${substr(var.prefix, 0, 25)}-site-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "90"
    protocol            = "HTTP"
    matcher             = "200-299"
    timeout             = "20"
    path                = "/"
    unhealthy_threshold = "2"
  }

  depends_on = [
    aws_lb.alb
  ]
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = var.dns ? "redirect" : "forward"
    target_group_arn = aws_lb_target_group.site_target_group.arn
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https_listener" {
  count             = var.dns ? 1 : 0
  load_balancer_arn = aws_lb.alb.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn   = try(aws_acm_certificate_validation.cert_validation[0].certificate_arn, null)

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "Incorrect path"
      status_code  = "503"
    }
  }
}

resource "aws_lb_listener_rule" "site_listener_rule" {
  listener_arn = var.dns ? aws_lb_listener.https_listener[0].arn : aws_lb_listener.http_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.site_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

}